from src.train import train
from src.model import RustCls
from src.device import device
import torch


def main():
    rust_cls = RustCls()
    model_path = "pths/model_52.pth"

    if model_path is not None:
        rust_cls.load_state_dict(torch.load(model_path, map_location=device))

    rust_cls = rust_cls.to(device)
    rust_cls.train()

    # start_epoch = 1,
    # start_lr = 1e-2,
    # last_lr = 1e-5,
    # batch_size = 64,
    # opt = "sgd",
    # epochs = 100,
    # epoch_to_unfreeze = 100

    # start_epoch = 53,
    # start_lr = 1e-4,
    # last_lr = 1e-5,
    # batch_size = 16, # memory concern
    # opt = "sgd",
    # epochs = 20,
    # epoch_to_unfreeze = 53

    train(
        rust_cls,
        start_epoch=53,
        start_lr=1e-4,
        last_lr=1e-5,
        batch_size=16,
        opt="sgd",
        epochs=20,
        epoch_to_unfreeze=53
    )


if __name__ == "__main__":
    main()
