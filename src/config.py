import os

proj_dir = os.path.dirname(os.path.dirname(__file__))

training_normal_dir = "signboard_classification_data/erased/training/normal"
training_rust_dir = "signboard_classification_data/erased/training/rust"

validation_normal_dir = "signboard_classification_data/erased/validation/normal"
validation_rust_dir = "signboard_classification_data/erased/validation/rust"

testing_normal_dir = "signboard_classification_data/erased/testing/normal"
testing_rust_dir = "signboard_classification_data/erased/testing/rust"

input_width = 224
input_height = 224
vgg_norm_mean = [0.485, 0.456, 0.406]
vgg_norm_std = [0.229, 0.224, 0.225]

performance_graph_dir = os.path.join(proj_dir, "graph")

pths_dir = os.path.join(proj_dir, "pths")
save_json_path = os.path.join(
    proj_dir, "result.json"
)

serve_model_path = os.path.join(proj_dir, "final_pth", "model.pth")
