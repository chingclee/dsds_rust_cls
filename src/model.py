import torch
import torch.nn as nn
import torch.nn.functional as F
# from torchsummary import summary
from torchvision.models import resnet50
from torchsummary import summary
from .device import device


class RustCls(nn.Module):
    def __init__(self):
        super(RustCls, self).__init__()

        self.backbone = resnet50(pretrained=True)
        self.blk1 = nn.Sequential(
            self.backbone.conv1,
            self.backbone.bn1,
            self.backbone.relu,
            self.backbone.maxpool,
            self.backbone.layer1
        )
        self.freeze_backbone()
        self.upscale = lambda x: F.interpolate(x, scale_factor=2)
        self.lateral_conv_4 = nn.Conv2d(2048, 256, 1, 1)
        self.lateral_conv_3 = nn.Conv2d(1024, 256, 1, 1)
        self.lateral_conv_2 = nn.Conv2d(512, 256, 1, 1)
        self.lateral_conv_1 = nn.Conv2d(256, 256, 1, 1)

        inter_feat_dim = 1

        self.linear_1 = nn.Linear(56*56, inter_feat_dim)
        self.linear_2 = nn.Linear(28*28, inter_feat_dim)
        self.linear_3 = nn.Linear(14*14, inter_feat_dim)
        self.linear_4 = nn.Linear(7*7, inter_feat_dim)
        self.final_linear = nn.Sequential(
            nn.Linear(1024, 512),
            nn.Dropout(p=0.5),
            nn.Linear(512, 128),
            nn.Dropout(p=0.5),
            nn.Linear(128, 2)
        )

    def freeze_backbone(self):
        for param in self.backbone.parameters():
            param.requires_grad = False

    def unfreeze_backbone(self):
        for param in self.backbone.parameters():
            param.requires_grad = True

    def forward(self, x):
        batches = x.shape[0]
        c1 = self.blk1(x)
        c2 = self.backbone.layer2(c1)
        c3 = self.backbone.layer3(c2)
        c4 = self.backbone.layer4(c3)

        p4 = self.lateral_conv_4(c4)
        p3 = self.upscale(p4) + self.lateral_conv_3(c3)
        p2 = self.upscale(p3) + self.lateral_conv_2(c2)
        p1 = self.upscale(p2) + self.lateral_conv_1(c1)

        logit1 = self.linear_1(p1.view(-1, 256, 56*56)).view((batches, -1))
        logit2 = self.linear_2(p2.view(-1, 256, 28*28)).view((batches, -1))
        logit3 = self.linear_3(p3.view(-1, 256, 14*14)).view((batches, -1))
        logit4 = self.linear_4(p4.view(-1, 256, 7*7)).view((batches, -1))
        logits = torch.cat([logit1, logit2, logit3, logit4], dim=1)
        logits = self.final_linear(logits)

        return logits


if __name__ == "__main__":
    rust_cls = RustCls().to(device)
    img = torch.randn((1, 3, 224, 224)).to(device)
    logit = rust_cls(img)
    print(logit.shape)
