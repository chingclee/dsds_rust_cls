from torch.utils.data import Dataset
from . import config
from glob import glob
from PIL import Image
from torchvision import transforms
import numpy as np
import random
import albumentations as A
import cv2


def create_albumentation_transform(prob=1):
    albumentation_transform = A.Compose([
        A.RandomBrightnessContrast(p=0.7*prob),
        A.ImageCompression(quality_lower=40, quality_upper=100, p=1*prob),
        A.ShiftScaleRotate(
            shift_limit=0,
            scale_limit=[0.7, 1.1],
            rotate_limit=[-20, 20],
            border_mode=cv2.BORDER_CONSTANT,
            value=0,
            p=0.8*prob
        ),
        A.OneOf([
            A.Blur(blur_limit=2, p=0.5),
        ], p=1.0*prob)
    ])
    return albumentation_transform


defect_transform = create_albumentation_transform()
normal_transform = create_albumentation_transform(0.2)
torch_img_transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize(mean=config.vgg_norm_mean,
                         std=config.vgg_norm_std)
])


def resize_img(img):
    """
    img:  Pillow image
    """
    h, w = img.height, img.width
    if h >= w:
        ratio = config.input_height / h
        new_h, new_w = int(h * ratio), int(w * ratio)
    else:
        ratio = config.input_height / h
        new_h, new_w = int(h * ratio), int(w * ratio)

        if new_w > config.input_width:
            ratio = config.input_width / new_w
            new_h, new_w = int(new_h * ratio), int(new_w * ratio)

    img = img.resize((new_w, new_h), Image.BILINEAR)
    return img, (w, h)


def pad_img(img):
    h = img.height
    w = img.width
    img = np.array(img)
    img = np.pad(img, pad_width=((0, config.input_height - h),
                 (0, config.input_width - w), (0, 0)))
    img = Image.fromarray(img)
    assert img.height == config.input_height
    assert img.width == config.input_width
    return img


def resize_and_padding(img, return_window=False):
    """
    img: Pil Image
    """
    img, (ori_w, ori_h) = resize_img(img)
    w = img.width
    h = img.height
    padding_window = (w, h)
    img = pad_img(img)

    if not return_window:
        return img
    else:
        return img, padding_window, (ori_w, ori_h)


class RustClsDataset(Dataset):
    def __init__(self, mode):
        """
        mode: "train", "test" or "validation"
        """
        assert mode in ["train", "test", "validation"]
        super(RustClsDataset, self).__init__()

        if mode == "train":
            normal_dir = config.training_normal_dir
            rust_dir = config.training_rust_dir
        elif mode == "test":
            normal_dir = config.testing_normal_dir
            rust_dir = config.testing_rust_dir
        elif mode == "validation":
            normal_dir = config.validation_normal_dir
            rust_dir = config.validation_rust_dir

        self.mode = mode

        normal_img_paths = [[0] + [path]
                            for path in glob(f"{normal_dir}/*.jpg")]
        rust_img_paths = [[1] + [path]
                          for path in glob(f"{rust_dir}/*.jpg")]
        multiple = len(normal_img_paths) // len(rust_img_paths)
        # balance the number of chance the sample being taken
        # we assume the proportion of data (normal:rust = 5:2):
        # 1:1 is not helpful as it does not match the real world data
        rust_img_paths = rust_img_paths * max(int(0.4 * multiple), 1)
        self.index_path = normal_img_paths + rust_img_paths
        random.shuffle(self.index_path)

    def __getitem__(self, idx):
        index, img_path = self.index_path[idx]
        img = np.array(Image.open(img_path))
        if self.mode == "train":
            if index == 1:
                img = defect_transform(image=img)["image"]
            else:
                img = defect_transform(image=img)["image"]

        img = resize_and_padding(Image.fromarray(img))
        img = torch_img_transform(img)
        return index, img

    def __len__(self):
        return len(self.index_path)
