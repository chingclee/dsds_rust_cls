from torch.utils.data import DataLoader
from .dataset import RustClsDataset
from .utils import ConsoleLog
from . import config
from .device import device
from pydash import get, set_

import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import json
import os
from pathlib import Path


def get_accuracy(cls_model: nn.Module, mode="validation"):
    """
    mode: "test", "validation"
    """
    assert mode in ["train", "test", "validation"]
    cls_model.eval()

    rust_cls_dataset = RustClsDataset(mode=mode)
    data_loader = DataLoader(
        rust_cls_dataset, batch_size=16, drop_last=False
    )
    total_num_data = len(rust_cls_dataset)

    total_correct_cls = 0

    for data in data_loader:
        indexes, imgs = data
        indexes = indexes.float().to(device)
        imgs = imgs.to(device)
        logits = cls_model(imgs)
        predicted_class = torch.argmax(logits, dim=1)
        correct_cls_mask = predicted_class == indexes
        num_of_correct_cls = torch.sum(correct_cls_mask.int())
        total_correct_cls += num_of_correct_cls

    accuracy = total_correct_cls / total_num_data

    cls_model.train()

    return accuracy.detach().cpu().item()


def truncate_result_json(start_epoch=1, json_path=""):
    if os.path.exists(json_path):
        with open(json_path, "r") as f:
            try:
                result_json = json.load(f)
            except BaseException:
                result_json = {
                    "train_acc": [],
                    "val_acc": []
                }
        for key, data in result_json.items():
            result_json[key] = data[0:start_epoch - 1]

        with open(json_path, "w") as f:
            json.dump(result_json, f, indent=4)


def save_performance_json(cls_model, save_json_path=""):
    result_json = None

    if not os.path.exists(save_json_path):
        Path(save_json_path).touch()

    with open(save_json_path, "r") as f:
        try:
            result_json = json.load(f)
        except BaseException:
            result_json = {
                "train_acc": [],
                "val_acc": []
            }

        train_acc = get_accuracy(cls_model, mode="train")
        val_acc = get_accuracy(cls_model, mode="validation")

        with open(save_json_path, "w") as f:
            result_json["train_acc"].append(train_acc)
            result_json["val_acc"].append(val_acc)
            json.dump(result_json, f, indent=4)

    return result_json


def plot_accuracy_graph(
    json_data: dict,
    ylabel="Accuracy",
    save_graph_at=""
):
    """
    json_data: all values are list of numbers of the same length
    """
    plt.style.use("ggplot")

    epochs = len(list(json_data.values())[0])
    N = np.arange(0, epochs)
    N_values = N + 1
    plt.figure()
    plt.gca().xaxis.set_major_locator(ticker.MultipleLocator(5))
    plt.title("Training and Validation Accuracy [Epoch {}]".format(
        len(N)
    ))
    for key, data in json_data.items():
        plt.plot(N, data, label=key)

    plt.xlabel("Epoch #")
    plt.xticks(N, N_values)
    plt.ylabel(ylabel)
    plt.legend()

    # save the figure
    plt.savefig(save_graph_at)
    dirname = os.path.dirname(save_graph_at)
    plt.savefig(os.path.join(dirname, "latest.png"))
    plt.close()
