from sympy import beta
from tqdm import tqdm
from torch.utils.data import DataLoader
from .dataset import RustClsDataset
from .utils import ConsoleLog
from .model import RustCls
from .device import device
from .performance import get_accuracy, plot_accuracy_graph, save_performance_json, truncate_result_json
from . import config
from pydash import get, set_
import torch.nn as nn
import numpy as np
import torch
import os
import json
from typing import cast

cce = nn.CrossEntropyLoss()
console_log = ConsoleLog(lines_up_on_end=1)


def train(
    cls_model: RustCls,
    start_epoch=1,
    epochs=10,
    start_lr=1e-3,
    last_lr=1e-5,
    batch_size=16,
    opt="adam",
    epoch_to_unfreeze=10
):
    assert opt in ["adam", "sgd"]

    truncate_result_json(
        start_epoch=start_epoch,
        json_path=config.save_json_path
    )

    if opt == "sgd":
        opt = torch.optim.SGD(cls_model.parameters(),
                              lr=start_lr, momentum=0.9)
    elif opt == "adam":
        opt = torch.optim.Adam(cls_model.parameters(), lr=start_lr)

    rust_dataset = RustClsDataset(mode="train")
    data_loader = DataLoader(rust_dataset, batch_size=batch_size, shuffle=True)

    lr_decay = np.power(last_lr / start_lr, 1 / (epochs - 1))
    lr_scheduler = torch.optim.lr_scheduler.ExponentialLR(
        opt,
        gamma=lr_decay
    )

    # unfreeze after 10 epochs
    cls_model.freeze_backbone()
    weight_freezed = True

    for epoch in range(epochs):
        epoch = epoch + start_epoch

        if weight_freezed and epoch >= epoch_to_unfreeze:
            cls_model.unfreeze_backbone()
            weight_freezed = False

        for data in tqdm(data_loader, initial=1, desc=f"Epoch {epoch}"):
            indexes, imgs = data
            indexes = cast(torch.Tensor, indexes).long().to(device)
            imgs = imgs.to(device)
            pred_scores = cls_model(imgs)
            loss = cce(pred_scores, indexes)

            opt.zero_grad()
            loss.backward()
            opt.step()

            with torch.no_grad():
                console_log.print([
                    ("loss", loss.item()),
                    ("lr", lr_scheduler.get_last_lr()[-1])
                ])
        lr_scheduler.step()

        result_json = save_performance_json(
            cls_model,
            save_json_path=config.save_json_path,
        )

        plot_accuracy_graph(
            result_json,
            ylabel="Accuracy",
            save_graph_at=os.path.join(
                config.performance_graph_dir, f"epoch_{epoch}"
            )
        )

        if not os.path.exists(config.pths_dir):
            os.makedirs(config.pths_dir)

        if epoch % 4 == 0:
            state_dict = cls_model.state_dict()
            torch.save(
                state_dict,
                os.path.join(config.pths_dir, f"model_{epoch}.pth")
            )
