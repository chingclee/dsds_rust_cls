1. Open `release/python` as the current working directory
2. run `git submodule add https://gitlab.com/chingclee/dsds_rust_cls.git models/dsds_rust_cls` in terminal
3. A submodule is added in `release/python/models/dsds_rust_cls`
4. After pulling the repo, we need to add folder `final_pth` (same level as `src`) in the working directory and copy the weight `model.pth`.
5. The weight `model.pth` can be found in `/home/raspect/Workspaces/jameslee/ai-master/release/python/tasks/dsds_rust_cls/final_pth/model.pth` (`cp` it into your working package of `dsds_rust_cls/final_pth`)
6. On submodules: Note that if `ai-master` is pulled from scratch, then `git submodule update --init` is needed, otherwise the folder `dsds_rust_cls` will be pulled only as an empty shell.