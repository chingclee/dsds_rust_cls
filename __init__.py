from .src import config
from .src.model import RustCls
from .src.device import device
from .src.dataset import resize_and_padding, torch_img_transform
from typing import cast
import torch


class RustEvaluation:
    rust_cls_model = None

    def get_model(self):
        if RustEvaluation.rust_cls_model is None:
            rust_cls = RustCls()
            rust_cls.load_state_dict(
                torch.load(config.serve_model_path)
            )
            rust_cls = rust_cls.to(device)
            rust_cls.eval()

            RustEvaluation.rust_cls_model = rust_cls
        return RustEvaluation.rust_cls_model

    def evaluate(self, img):
        """
        img: Pil Image
        """
        img = resize_and_padding(img)
        img = torch_img_transform(img).to(device)[None, ...]

        rust_cls_model = self.get_model()
        logits = rust_cls_model(img)
        score = cast(
            torch.Tensor,
            logits
        ).softmax(dim=1)[0].detach().cpu().numpy()

        rust_score = score[1]
        return rust_score
